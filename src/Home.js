import React from 'react'
import "./Home.css"
import Product from './Product';

function Home() {
    return (
        <div className="home">
            <div className='home_container'>
                <img className="home_image" 
                src="https://m.media-amazon.com/images/I/61ASx7NHTWL._SX3000_.jpg" alt="banner"></img>

                <div className="home_row">
                    <Product 
                     id="12321341"
                     title="The Lean Startup: How Constant Innovation Creates Radically Successful Businesses Paperback"
                     price={11.96}
                     rating={5}
                     image="https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._SX325_BO1,204,203,200_.jpg"
                     />

                    <Product 
                     id="49538094"
                     title="Kenwood kMix Stand Mixer for Baking, Stylish Kitchen Mixer with K-beater, Dough Hook and Whisk, 5 Litre Glass Bowl"
                     price={239.0}
                     rating={4}
                     image="https://store.flyingblue.com/media/catalog/product/cache/d18cb01b41c18fc26e1046af99e0e2f4/k/m/kmx750rd_5_3_1.jpg"
                     />
                    
                </div>

                <div className="home_row">
                    <Product
                    id="4903850"
                    title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor"
                    price={199.99}
                    rating={3}
                    image="https://www.nunua-online.com/image/product/22979/MEDIUM/0/92179"
                    />
                    <Product
                    id="23445930"
                    title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
                    price={98.99}
                    rating={5}
                    image="https://www.wordstream.com/wp-content/uploads/2021/07/amazon-seo-product-images.jpg"
                     />
                    
                    <Product 
                    id="3254354345"
                    title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
                    price={598.99}
                    rating={4}
                    image="https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/ipad-pro-12-11-select-202104_FMT_WHH?wid=2000&hei=2000&fmt=jpeg&qlt=80&.v=1617067383000"
                     />
                    
                </div> 

                <div className="home_row">
                <Product 
                  id="90829332"
                  title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor - Super Ultra Wide Dual WQHD 5120 x 1440"
                  price={1094.98}
                  rating={4}
                  image="https://m.media-amazon.com/images/I/81rus0UFhsL._AC_SX450_.jpg" 
                     />
                    
                </div>
            </div>
        </div>
    );
}

export default Home;
