import firebase from "firebase/compat/app";
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// Import the functions you need from the SDKs you need
//import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDG6CDbKYCkrqaEB_PCQaWKgTHhyVztzwQ",
  authDomain: "sahara-5a210.firebaseapp.com",
  projectId: "sahara-5a210",
  storageBucket: "sahara-5a210.appspot.com",
  messagingSenderId: "657243539356",
  appId: "1:657243539356:web:c35c5eddfa42556e646a59",
  measurementId: "G-1X8NG54C96"
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

//eslint-disable-next-line no-unused-vars
const analytics = getAnalytics(firebaseApp);

export { db, auth };