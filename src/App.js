//import { Home } from '@material-ui/icons';
import React, { useEffect } from 'react';
import './App.css';
import Header from './Header';
import UsingHooks from './UsingHooks';
import Home from './Home';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Checkout from "./Checkout";
import Payment from './Payment';
import Login from './Login';
import { auth } from "./firebase";
import { useStateValue } from "./StateProvider";


function App() {
  const [{}, dispatch] = useStateValue();

  useEffect(() => {
    //will only run once when the app component loads...
    auth.onAuthStateChanged(authUser => {
      console.log('The user is >>> ', authUser);

      if (authUser) {
        //the user just logged in/ the user was logged in

        dispatch({
          type: 'SET_USER',
          user: authUser

        });
      } else {
        //the user is logged out

        
        dispatch({
          type: 'SET_USER',
          user: null

        });
      }
    });
  }, [dispatch]);

  return (
    //BEM
    <Router>
      <div className="app">
      
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/Checkout">
            <Header />
            <Checkout />
          </Route>
          <Route path="/payment">
            <Header />
            <Payment />
            <UsingHooks />
          </Route>
          <Route path="/">  
            <Header />         
            <Home />
          </Route>
        </Switch>
    </div>
    </Router>
  );
}

export default App;
